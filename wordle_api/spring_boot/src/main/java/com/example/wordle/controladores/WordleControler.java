package com.example.wordle.controladores;

import java.util.List;

import com.example.wordle.controladores.mensajes.PistasRequest;
import com.example.wordle.servicios.WordleService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/wordle")
public class WordleControler {

    private final WordleService wordleService;

    public WordleControler(WordleService wordleService) {
        this.wordleService = wordleService;
    }

    @PostMapping("/posibles")
    public List<String> obtenerPosibles(@RequestBody PistasRequest pistas) {
        return wordleService.carcularPalabrasPosibles(pistas);
    }

    @PostMapping("/palabra-correcta")
    public void palabraCorrecta(@RequestBody String palabra) {
    }

    @PostMapping("/no-es-palabra")
    public void noEsPalabra(@RequestBody String palabra) {
    }
}
