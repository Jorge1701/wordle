package com.example.wordle.repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.springframework.stereotype.Service;

@Service
public class WordleRepo {
    
    public DSLContext obtenerContexto() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "postgres");
            return DSL.using(conn, SQLDialect.POSTGRES);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<String> mapResultado(Result<Record1<String>> resultado) {
        return resultado.stream().map(fila -> fila.getValue(0, String.class)).toList();
    }
}
