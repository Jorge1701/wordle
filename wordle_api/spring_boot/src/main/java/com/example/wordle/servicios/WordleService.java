package com.example.wordle.servicios;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

import com.example.wordle.controladores.mensajes.PistasRequest;
import com.example.wordle.repositorio.WordleRepo;

import org.jooq.Condition;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;
import org.jooq.Table;

import static java.lang.String.format;
import static java.util.Collections.frequency;
import static java.util.Collections.singleton;
import static java.util.Optional.ofNullable;
import static java.util.stream.IntStream.range;
import static org.jooq.impl.DSL.*;
import org.springframework.stereotype.Service;

@Service
public class WordleService {
    private final Table<Record> tabla = table("wordle.palabra").as("p");
    private final Field<String> campoPalabra = field(name("p", "palabra"), String.class);

    private final WordleRepo wordleRepo;

    public WordleService(WordleRepo wordleRepo) {
        this.wordleRepo = wordleRepo;
    }

    public List<String> carcularPalabrasPosibles(PistasRequest pistas) {
        return wordleRepo.mapResultado(ejecutarConsulta(pistas));
    }

    private Result<Record1<String>> ejecutarConsulta(PistasRequest pistas) {
        Collection<Condition> condiciones = new LinkedList<>();

        // Solo buscar entre las palabras del largo correcto
        condiciones.add(length(campoPalabra).eq(pistas.getCorrectas().size()));

        // Condiciones de letras correctas
        range(0, pistas.getCorrectas().size()).forEach(index -> {
            String letraCorrecta = pistas.getCorrectas().get(index);
            // Si hay una letra correcta para el índice se agrega la condición de igualdad
            ofNullable(letraCorrecta).ifPresent(letra -> condiciones.add(substring(campoPalabra, index + 1, 1).eq(letra)));
        });

        // Condiciones para que la palabra tenga las letras posibles
        final Map<String, Integer> letrasPosiblesCantidad = new HashMap<>();
        pistas.getPosibles().forEach(letrasPosibles -> {
            letrasPosibles.stream().filter(Objects::nonNull).forEach(letraPosible -> {
                int frecuencia = frequency(letrasPosibles, letraPosible);

                if ((letrasPosiblesCantidad.containsKey(letraPosible) && letrasPosiblesCantidad.get(letraPosible) < frecuencia)
                    || !letrasPosiblesCantidad.containsKey(letraPosible)) {
                    letrasPosiblesCantidad.put(letraPosible, frecuencia);
                }
            });
        });

        letrasPosiblesCantidad.forEach((letraPosible, cantidad) -> {
            condiciones.add(campoPalabra.likeIgnoreCase(format("%s%%", format("%%%s", letraPosible).repeat(cantidad))));
        });

        // Condiciones para que la palabra no tenga las letras posibles donde se descubrieron
        Map<Integer, List<String>> letrasPosiblesPorPosicionEncontrada = new HashMap<>();
        pistas.getPosibles().stream().forEach(letrasPosibles -> {
            range(0, letrasPosibles.size()).forEach(index -> {
                if (letrasPosibles.get(index) == null) {
                    return;
                }
                if (letrasPosiblesPorPosicionEncontrada.containsKey(index)) {
                    letrasPosiblesPorPosicionEncontrada.get(index);
                } else {
                    letrasPosiblesPorPosicionEncontrada.put(index, new ArrayList<>(singleton(letrasPosibles.get(index))));
                }
            });
        });

        // Si se encontraron letras posibles en una posición significa que no pueden ir ahí
        // de lo contrario serían letras correctas
        letrasPosiblesPorPosicionEncontrada.forEach((posicion, letrasPosibles) -> {
            condiciones.add(substring(campoPalabra, posicion + 1, 1).notIn(letrasPosibles));
        });

        // Condición letras incorrectas
        List<String> letrasIncorrectas = pistas.getIncorrectas().stream().filter(letraIncorrecta ->
            !letrasPosiblesCantidad.keySet().contains(letraIncorrecta) || pistas.getCorrectas().contains(letraIncorrecta)
        ).toList();

        if (!letrasIncorrectas.isEmpty()) {
            IntStream.range(0, pistas.getCorrectas().size()).forEach(posicion -> {
                if (pistas.getCorrectas().get(posicion) == null) {
                    condiciones.add(substring(campoPalabra, posicion + 1, 1).notIn(letrasIncorrectas));
                }
            });
        }

        return wordleRepo.obtenerContexto()
            .select(campoPalabra)
            .from(tabla)
            .where(condiciones)
            .fetch();
    }
}
