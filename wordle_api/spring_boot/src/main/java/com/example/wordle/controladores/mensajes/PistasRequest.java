package com.example.wordle.controladores.mensajes;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PistasRequest {
    private final List<String> correctas;
    private final List<List<String>> posibles;
    private final List<String> incorrectas;

    @JsonCreator
    public PistasRequest(
        @JsonProperty("correctas") List<String> correctas,
        @JsonProperty("posibles") List<List<String>> posibles,
        @JsonProperty("incorrectas") List<String> incorrectas
    ) {
        this.correctas = correctas;
        this.posibles = posibles;
        this.incorrectas = incorrectas;
    }

    public List<String> getCorrectas() {
        return correctas;
    }

    public List<List<String>> getPosibles() {
        return posibles;
    }

    public List<String> getIncorrectas() {
        return incorrectas;
    }
}
