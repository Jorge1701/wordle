import os
from controlador_db import ejecutar

LARGO_MIN = 3
LARGO_MAX = 8

# Crear esquema si no existe
ejecutar('CREATE SCHEMA IF NOT EXISTS wordle')
ejecutar('''
CREATE TABLE IF NOT EXISTS wordle.palabra (
	palabra varchar NOT NULL,
	CONSTRAINT palabra_pk PRIMARY KEY (palabra)
)
''')
# Vaciar tabla de palabras
ejecutar('TRUNCATE wordle.palabra')

# Inserta todas las palabras de la lista en la tabla palabras.palabras
def insertar_palabras(palabras) :
    palabras_juntas = ','.join(["('{}')".format(palabra) for palabra in palabras])
    ejecutar(
        '''
        INSERT INTO wordle.palabra (
            palabra
        ) VALUES {}
        ON CONFLICT DO NOTHING
        '''.format(palabras_juntas)
    )

# Lee todas las palabras del archivo y las inserta de a 1000
archivo_palabras = os.path.join(os.path.dirname(__file__), '..\\..\\palabras\\es.txt')
palabras = open(archivo_palabras, 'r')
palabras_batch = []
for palabra in palabras :
    palabra = palabra.lower().strip()
    if len(palabra) >= LARGO_MIN and len(palabra) <= LARGO_MAX :
        palabras_batch.append(palabra)
    
    if len(palabras_batch) >= 1000 :
        insertar_palabras(palabras_batch)
        palabras_batch = []

insertar_palabras(palabras_batch)

print('Palabras cargadas!')
