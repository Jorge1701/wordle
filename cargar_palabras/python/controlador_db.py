import psycopg2

def conexion() :
    try :
        return psycopg2.connect(
            host="localhost",
            database="postgres",
            user="postgres",
            password="postgres"
        )
    except (Exception, psycopg2.DatabaseError) as e :
        print('Error al conectar con DB: ', e)

def ejecutar(sql) :
    with conexion() as conn :
        with conn.cursor() as cursor :
            cursor.execute(sql)
