export type Estado = 'correcta' | 'incorrecta' | 'posible';

export interface EstadoLetra {
    letra: string;
    estado: Estado;
}
