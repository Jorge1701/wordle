import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OpcionesService {
  // Letras permitidas
  private letrasPermitidas: string = "qwertyuiopasdfghjklzxcvbnm";
  // Opciones disponibles
  private idiomasDisponibles: string[] = ['Español', 'Inglés', 'Portugués'];

  private cantidadLetrasMinima: number = 3;
  private cantidadLetrasMaxima: number = 8;

  // Valores actuales
  private idiomaSeleccionado: string = this.idiomasDisponibles[0];
  private cantidadLetras: number = 5;

  public getLetrasPermitidas(): string {
    return this.letrasPermitidas;
  }

  public getIdiomasDisponibles(): string[] {
    return this.idiomasDisponibles;
  }

  public getCantidadLetrasMinima(): number {
    return this.cantidadLetrasMinima;
  }

  public getCantidadLetrasMaxima(): number {
    return this.cantidadLetrasMaxima;
  }

  public getIdiomaSeleccionado(): string {
    return this.idiomaSeleccionado;
  }

  public setIdiomaSeleccionado(valor: string): void {
    this.idiomaSeleccionado = valor;
    console.log('Idioma cambiado', this.idiomaSeleccionado);
  }
  
  public getCantidadLetras(): number {
    return this.cantidadLetras;
  }

  public setCantidadLetras(valor: number): void {
    this.cantidadLetras = valor;
    console.log('Cantidad letras cambiada', this.cantidadLetras);
  }
}
