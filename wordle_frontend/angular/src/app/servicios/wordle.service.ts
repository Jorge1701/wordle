import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WordleService {

  constructor(
    private http: HttpClient) {
  }

  public obtenerPosibilidades(correctas: string[], posibles: string[][], incorrectas: string[]): Observable<string[]> {
    return this.http.post<string[]>(environment.apiUrl + '/posibles', { correctas, posibles, incorrectas });
  }

  public marcarPalabraCorrecta(palabra: string): Observable<void> {
    return this.http.post<void>(environment.apiUrl + '/palabra-correcta', palabra);
  }

  public noEsPalabra(palabra: string): Observable<void> {
    return this.http.post<void>(environment.apiUrl + '/no-es-palabra', palabra);
  }
}
