import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html'
})
export class DialogoComponent {
  titulo: string = 'Confirmar';
  mensaje: string = '¿Confirmar acción?';

  constructor(
    @Inject(MAT_DIALOG_DATA) public info: any
  ) {
    this.titulo = info.titulo;
    this.mensaje = info.mensaje;
  }
}
