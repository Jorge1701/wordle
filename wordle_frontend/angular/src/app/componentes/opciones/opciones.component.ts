import { Component, OnInit } from '@angular/core';
import { OpcionesService } from 'src/app/servicios/opciones.service';

@Component({
  selector: 'app-opciones',
  templateUrl: './opciones.component.html',
  styleUrls: ['./opciones.component.sass']
})
export class OpcionesComponent implements OnInit {

  constructor(
    private opcionesService: OpcionesService
  ) {}

  ngOnInit(): void {
    console.log(this.opcionesService.getIdiomaSeleccionado());
    console.log(this.opcionesService.getCantidadLetras());
  }

  cambiarCantidad(valor: number): void {
    this.cantidadLetras = valor;
  }

  get idiomaSeleccionado(): string {
    return this.opcionesService.getIdiomaSeleccionado();
  }

  set idiomaSeleccionado(valor: string) {
    this.opcionesService.setIdiomaSeleccionado(valor);
  }

  get cantidadLetras(): number {
    return this.opcionesService.getCantidadLetras();
  }

  set cantidadLetras(valor: number) {
    this.opcionesService.setCantidadLetras(valor);
  }

  get idiomasDisponibles(): string[] {
    return this.opcionesService.getIdiomasDisponibles();
  }

  get cantidadLetrasPosibilidades(): number[] {
    const cant: number = this.opcionesService.getCantidadLetrasMaxima() - this.opcionesService.getCantidadLetrasMinima() + 1;
    return Array(cant).fill(0).map((_, i) => i + this.opcionesService.getCantidadLetrasMinima());
  }
}
