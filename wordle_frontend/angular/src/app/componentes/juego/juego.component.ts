import { Component, OnInit } from '@angular/core';
import { OpcionesService } from 'src/app/servicios/opciones.service';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.sass']
})
export class JuegoComponent implements OnInit {

  constructor(
    private opcionesService: OpcionesService
  ) {}

  ngOnInit(): void {
    console.log('Juego', this.opcionesService.getIdiomaSeleccionado(), this.opcionesService.getCantidadLetras());
  }
}
