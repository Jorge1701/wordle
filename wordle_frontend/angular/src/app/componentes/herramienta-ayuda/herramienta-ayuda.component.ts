import { Component, HostListener, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';
import { Estado, EstadoLetra } from 'src/app/modelo/estado-letra';
import { OpcionesService } from 'src/app/servicios/opciones.service';
import { WordleService } from 'src/app/servicios/wordle.service';
import { DialogoComponent } from '../dialogo/dialogo.component';

const TECLA_ENTER: string = 'Enter';
const TECLA_BORRAR: string = 'Backspace';
const ESPACIO_VACIO: string = ' ';

@Component({
  selector: 'app-herramienta-ayuda',
  templateUrl: './herramienta-ayuda.component.html',
  styleUrls: ['./herramienta-ayuda.component.sass']
})
export class HerramientaAyudaComponent implements OnInit {

  cantidadLetras: number = this.opcionesService.getCantidadLetras();
  palabras: EstadoLetra[][] = [Array(this.cantidadLetras).fill({})];

  resultadosBusqueda: string[] = [];
  noSonPalabras: string[] = [];
  palabraCorrecta: string = '';

  private mensaje: MatSnackBarRef<TextOnlySnackBar> | null = null;

  constructor(
    private opcionesService: OpcionesService,
    private snackBar: MatSnackBar,
    private wordleService: WordleService,
    private matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.reiniciar();
  }

  marcarPalabraCorrecta(palabra: string): void {
    this.matDialog.open(DialogoComponent, {
      data: {
        titulo: 'Marcar palabra correcta',
        mensaje: `¿La palabra que buscaba era '${palabra}'?`
      }
    }).afterClosed().subscribe(confirmado => {
      if (confirmado) {
        this.wordleService.marcarPalabraCorrecta(palabra).subscribe(() => {
          this.palabraCorrecta = palabra;
          const letras: EstadoLetra[] = palabra.split('').map(l => {
            return { letra: l, estado: 'correcta' };
          });
          this.palabras.push(letras);
        });
      }
    });
  }

  noEsPalabra(palabra: string): void {
    this.matDialog.open(DialogoComponent, {
      data: {
        titulo: 'No es una palabra',
        mensaje: `¿Está seguro que '${palabra}' no es una palabra en ${this.opcionesService.getIdiomaSeleccionado()}?`
      }
    }).afterClosed().subscribe(confirmado => {
      if (confirmado) {
        this.wordleService.noEsPalabra(palabra).subscribe(() => {
          this.noSonPalabras.push(palabra);
          this.snackBar.open('Gracias por reportar la palabra', undefined, { duration: 5000 });
        });
      }
    });
  }

  consultar(): void {
    if (this.palabraCorrecta !== '') {
      return;
    }

    let palabrasCompletas: boolean = true;
    let posicion = 0;
    while (posicion < this.palabras.length * this.cantidadLetras) {
      if (this.getLetra(posicion).letra === ESPACIO_VACIO) {
        palabrasCompletas = false;
        break;
      }
      posicion++;
    }
  
    if (palabrasCompletas) {
      if (this.mensaje !== null) {
        this.mensaje.dismiss();
      }

      let correctas: string[] = this.obtenerCorrectas();
      let posibles: string[][] = this.obtenerPosibles();
      let incorrectas: string[] = this.obtenerIncorrectas();

      this.wordleService.obtenerPosibilidades(correctas, posibles, incorrectas).subscribe((resultados) => {
        this.resultadosBusqueda = resultados;
      });
    } else {
      this.mensaje = this.snackBar.open('Complete todas las letras.', 'Ok'.toUpperCase(), { duration: 3000 });
    }
  }

  private obtenerCorrectas(): string[] {
    let correctas: string[] = Array(this.cantidadLetras).fill(null);
    for (let ip = 0; ip < this.palabras.length; ip++) {
      for (let il = 0; il < this.cantidadLetras; il++) {
        if (this.palabras[ip][il].estado === 'correcta') {
          correctas[il] = this.palabras[ip][il].letra;
        }
      }
    }
    return correctas;
  }

  private obtenerPosibles(): string[][] {
    let posibles: string[][] = [];
    for (let ip = 0; ip < this.palabras.length; ip++) {
      posibles.push(Array(this.cantidadLetras).fill(null));
      for (let il = 0; il < this.cantidadLetras; il++) {
        if (this.palabras[ip][il].estado === 'posible') {
          posibles[ip][il] = this.palabras[ip][il].letra;
        }
      }
    }
    return posibles;
  }

  private obtenerIncorrectas(): string[] {
    let incorrectas: string[] = [];
    for (let ip = 0; ip < this.palabras.length; ip++) {
      for (let il = 0; il < this.cantidadLetras; il++) {
        if (this.palabras[ip][il].estado === 'incorrecta') {
          incorrectas.push(this.palabras[ip][il].letra);
        }
      }
    }
    return incorrectas;
  }

  reiniciar(): void {
    this.palabras = [Array(this.cantidadLetras).fill({})];
    this.vaciarPalabra(0);
    this.resultadosBusqueda = [];
    this.palabraCorrecta = '';
  }

  cambiarEstado(ip: number, il: number): void {
    this.palabras[ip][il].estado = this.siguienteEstado(this.palabras[ip][il].estado);
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.palabraCorrecta !== '') {
      return;
    }

    if (event.key === TECLA_ENTER) {
      this.consultar();
      return;
    }

    let posicion = 0;
    while (posicion < this.palabras.length * this.cantidadLetras) {
      if (this.getLetra(posicion).letra === ESPACIO_VACIO) {
        break;
      }
      posicion++;
    }

    if (this.opcionesService.getLetrasPermitidas().includes(event.key)) {
      if (posicion === this.palabras.length * this.cantidadLetras) {
        this.palabras.push(Array(this.cantidadLetras));
        this.vaciarPalabra(this.palabras.length - 1);
      }
      
      this.setLetra(posicion, event.key);
    } else if (event.key === TECLA_BORRAR) {
      this.setLetra(posicion - 1, ESPACIO_VACIO);

      if (posicion === (this.palabras.length - 1) * this.cantidadLetras) {
        this.palabras = [...this.palabras.slice(0, this.palabras.length - 1)];
      }
    }
  }

  private vaciarPalabra(posicion: number): void {
    for (let i = 0; i < this.cantidadLetras; i++) {
      this.palabras[posicion][i] = {
        letra: ESPACIO_VACIO,
        estado: 'incorrecta'
      };
    }
  }

  private getLetra(posicion: number): EstadoLetra {
    return this.palabras[Math.floor(posicion / this.cantidadLetras)][posicion % this.cantidadLetras];
  }

  private setLetra(posicion: number, letra: string): void {
    this.palabras[Math.floor(posicion / this.cantidadLetras)][posicion % this.cantidadLetras].letra = letra;
  }

  private siguienteEstado(estado: Estado): Estado {
    if (estado == 'correcta') {
      return 'incorrecta';
    } else if (estado == 'incorrecta') {
      return 'posible';
    } else {
      return 'correcta';
    }
  }

  get tamanoLetra(): number {
    if (this.cantidadLetras == 3) {
      return 750;
    } else if (this.cantidadLetras == 4) {
      return 600;
    } else if (this.cantidadLetras == 5) {
      return 500;
    } else if (this.cantidadLetras == 6) {
      return 400;
    } else if (this.cantidadLetras == 7) {
      return 300;
    } else if (this.cantidadLetras == 8) {
      return 250;
    } else {
      return 100;
    }
  }
}
